SELECT dep_name as DepartmentName, COUNT(emp_full_name) as TotalPeople FROM (
	SELECT db01.Departments.ID as dep_id, db01.Departments.Name as dep_name, db01.Employees.FullName as emp_full_name, db01.Employees.ID as emp_id FROM db01.Employees
	LEFT JOIN db01.Departments ON db01.Departments.ID = db01.Employees.DepartmentID
	UNION ALL
	SELECT db02.Departments.ID, db02.Departments.Name, db02.Employees.FullName, db02.Employees.ID FROM db02.Employees
	LEFT JOIN db02.Departments ON db02.Departments.ID = db02.Employees.DepartmentID
	UNION ALL
	SELECT db03.Departments.ID, db03.Departments.Name, db03.Employees.FullName, db03.Employees.ID FROM db03.Employees
	LEFT JOIN db03.Departments ON db03.Departments.ID = db03.Employees.DepartmentID
	UNION ALL
	SELECT db04.Departments.ID, db04.Departments.Name, db04.Employees.FullName, db04.Employees.ID FROM db04.Employees
	LEFT JOIN db04.Departments ON db04.Departments.ID = db04.Employees.DepartmentID
) t
GROUP BY(dep_name)

-- Results:
-- Without Dep	38
-- Abel Warren	1
-- Alex Rowland	2
-- Angela Horn	2
-- Arlene Andersen	1
-- Arnold French	5
-- Austin Rivas	1
-- B	1
-- Bobbi Haynes	1
-- Bobbie Hill	1
-- Brandie Chase	3
-- Brent Hanson	1
-- Bridgett Woodard	1
-- Byron Allen	3
-- C	3
-- Cameron Miranda	1
-- Cari Dillon	1
-- Carla Compton	3
-- Caroline Nash	2
-- Clarissa Giles	2
-- Damien Massey	3
-- Darla Dalton	1
-- Deana Kline	1
-- Dianna Oliver	1
-- Dustin Clements	1
-- Dwayne Flynn	1
-- Dwight Patton	2
-- E	1
-- Elena Huber	1
-- Emma Anthony	3
-- Erick Valentine	1
-- Ernest Lam	1
-- F	1
-- Franklin Jones	1
-- G	1
-- Geoffrey Aguirre	2
-- Gretchen Mason	1
-- Heath Stafford	1
-- Heidi Petty	1
-- Irma Arnold	1
-- Ismael Holt	3
-- Ivan Humphrey	2
-- Jami Cuevas	2
-- Janice Payne	1
-- Jasmine Padilla	2
-- Jayson Shields	2
-- Jesse Glover	2
-- Joni Roberts	2
-- Judith Kane	5
-- Julian Heath	1
-- Kathleen Novak	1
-- Kellie Galvan	2
-- Kendra Stevenson	1
-- Keri Williams	2
-- Kirsten Dickerson	1
-- Kristen Wilson	1
-- Kurt Wade	2
-- Laura Caldwell	1
-- Lauren Spencer	3
-- Lawanda Noble	2
-- Lewis Maxwell	1
-- Lillian Duran	2
-- Lucas Rivers	2
-- Luz Olson	2
-- Melissa Atkinson	1
-- Morgan Simon	1
-- Moses Downs	1
-- Noah Nolan	4
-- Paige Cooper	3
-- Regina Blackburn	2
-- Rex Galloway	2
-- Robbie Baird	1
-- Rose Kirk	3
-- Ruth Simmons	1
-- Sheldon Stuart	3
-- Tamiko Novak	1
-- Teddy Hoover	2
-- Theodore Melton	1
-- Tim Taylor	1
-- Wallace Savage	1
-- Wesley Nichols	3